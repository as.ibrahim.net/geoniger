package com.models.beans;

public class Ville {
	
	private String name;
	private String status;
	private String ethnie;
	
	
	
	
	
	
	
	public Ville(String name, String status, String ethnie) {
		super();
		this.name = name;
		this.status = status;
		this.ethnie = ethnie;
	}
	public Ville() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Ville [name=" + name + ", status=" + status + ", ethnie=" + ethnie + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEthnie() {
		return ethnie;
	}
	public void setEthnie(String ethnie) {
		this.ethnie = ethnie;
	}
	
	

}
