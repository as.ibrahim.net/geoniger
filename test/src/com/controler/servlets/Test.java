package com.controler.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.models.beans.Ville;

/**
 * Servlet implementation class Test
 */
@WebServlet("/Test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DateTime date = new DateTime();
		Integer jourDuMois = date.getDayOfMonth();
		 org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern( "dd/MM/yyyy HH:mm:ss" );
	        String srtdate = date.toString( formatter );
		
		getParamFromUrl(request);
		usingTabVar(request);
		usingBeanObject(request);
		String message = "Du code java vers(le contrôleur vers la vue (JSP)";
		request.setAttribute("var", message);
		request.setAttribute("date", date);
		request.setAttribute("jour", jourDuMois);
		request.setAttribute("srtdate", srtdate);
		this.getServletContext().getRequestDispatcher("/WEB-INF/premier.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	public void getParamFromUrl(HttpServletRequest request) {
		String name = (String) request.getParameter("name");
		//int age = (int) reques.getAttribute("age");
		String town = (String)request.getParameter("town");
		request.setAttribute("name", name);
		//reques.setAttribute("age", age);
		request.setAttribute("town", town);
	}
	
	public void usingTabVar(HttpServletRequest request){
		String[] names = {"Moussa", "IBou", "Ali"};
		request.setAttribute("names", names);
	}
	
	public void usingBeanObject(HttpServletRequest request){
		
		Ville ville = new Ville();
		ville.setName("Tera");
		ville.setStatus("Departement");
		ville.setEthnie("Songhroy");
		request.setAttribute("ville", ville);
	}


}
